/obj/structure/table/stalker
	smooth = SMOOTH_TRUE
	canSmoothWith = list(/obj/structure/table/stalker)
	pass_flags = LETPASSTHROW

/obj/structure/table/stalker/wood
	desc = "Обычный дерев&#255;нный слегка пошарпанный стол."
	eng_desc = "Simple table."
	icon = 'icons/obj/smooth_structures/stol_stalker.dmi'
	icon_state = "stol"
	deconstructable = 0
	smooth = SMOOTH_TRUE
	canSmoothWith = list(/obj/structure/table/stalker/wood)

/obj/structure/table/stalker/wood/bar
	desc = "Самодельна&#255; барна&#255; стойка"
	icon = 'icons/obj/smooth_structures/stol_stalker_bar.dmi'
	icon_state = "bar"
	smooth = SMOOTH_TRUE
	canSmoothWith = list(/obj/structure/table/stalker/wood/bar)

/obj/structure/table/stalker/wood/bar100rentgen
	desc = "Качественна&#255; барна&#255; стойка"
	icon = 'icons/stalker/structure/bartables.dmi'
	icon_state = "table"
	smooth = SMOOTH_FALSE

/obj/structure/stalker/okno
	name = "Window"
	desc = "Старое дерев&#255;нное окно."
	eng_desc = "Old wooden window."
	icon = 'icons/stalker/decor2.dmi'
	pass_flags = LETPASSTHROW
	density = 1
	opacity = 0
	layer = 2.9
	var/busy = 0
	var/mob/tableclimber
	var/unpassable = 0
	var/can_climb = 1
		
/obj/structure/stalker/okno/attack_hand(mob/living/user)
	user.changeNext_move(CLICK_CD_MELEE)
	if(tableclimber && tableclimber != user)
		tableclimber.Weaken(2)
		tableclimber.visible_message("<span class='warning'>[tableclimber.name] has been knocked off the table", "You're knocked off the table!", "You see [tableclimber.name] get knocked off the table</span>")
	..()

/obj/structure/stalker/okno/CanPass(atom/movable/mover, turf/target, height=0)
	if(height==0)
		return 1

	if(istype(mover) && mover.checkpass(PASSTABLE))
		return 1
	if(locate(/obj/structure/stalker/okno) in get_turf(mover))
		return 0
	else
		return !density

/obj/structure/stalker/okno/MouseDrop_T(atom/movable/O, mob/user)
	..()
	if(!can_climb)
		return
	if(ismob(O) && user == O && ishuman(user))
		if(user.canmove)
			climb_table(user)
			return
	if ((!( istype(O, /obj/item/weapon) ) || user.get_active_hand() != O))
		return
	if(isrobot(user))
		return
	if(!user.drop_item())
		return
	if (O.loc != src.loc)
		step(O, get_dir(O, src))
	return

/obj/structure/stalker/okno/proc/climb_table(mob/user)
	if(unpassable)
		user << "<span class='warning'>Get out from here.</span>"
		return
	else
		src.add_fingerprint(user)
		user.visible_message("<span class='warning'>[user] starts climbing onto [src].</span>", \
									"<span class='notice'>You start climbing onto [src]...</span>")
		var/climb_time = 20
		if(user.restrained()) //Table climbing takes twice as long when restrained.
			climb_time *= 2.5
		tableclimber = user
		if(do_mob(user, user, climb_time))
			if(src.loc) //Checking if table has been destroyed
				if(user.forceMove(get_turf(src)))
					user.visible_message("<span class='warning'>[user] climbs onto [src].</span>", \
										"<span class='notice'>You climb onto [src].</span>")
					add_logs(user, src, "climbed onto")
				else
					user << "<span class='warning'>You fail to climb onto [src].</span>"
				tableclimber = null
				return 1
		tableclimber = null
		return 0

/obj/structure/stalker/okno/window1
	icon_state = "okno1"

/obj/structure/stalker/okno/window2
	icon_state = "okno2"

/obj/structure/stalker/okno/window3
	icon_state = "okno3"

/obj/structure/stalker/okno/window4
	icon_state = "okno4"