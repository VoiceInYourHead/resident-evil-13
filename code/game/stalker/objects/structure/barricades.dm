/obj/structure/barricade/stalker
	icon = 'icons/stalker/decor.dmi'
	var/debriss_type
	var/loot
	var/lootenable = 0
	var/doubleloot = 0

/obj/structure/barricade/stalker/take_damage(damage, leave_debris=1, message)
	health -= damage
	if(health <= 0)
		if(message)
			visible_message(message)
		else
			visible_message("<span class='warning'>\The [src] is smashed apart!</span>")
		if(leave_debris && debriss_type)
			new debriss_type(get_turf(src))
			if(lootenable)
				if(loot)
					var/lootspawn = pickweight(loot)
					if(doubleloot)
						new lootspawn(get_turf(src))
						lootspawn = pickweight(loot)
						new lootspawn(get_turf(src))
						qdel(src)
					else
						new lootspawn(get_turf(src))
						qdel(src)
		qdel(src)

/obj/structure/barricade/stalker/wood
	name = "wooden barricade"
	desc = "Забарикадированный досками проход."
	eng_desc = "Planks."
	icon_state = "zabitiy_proxod"
	health = 5000
	maxhealth = 5000

/obj/structure/barricade/stalker/wood/attack_hand(mob/user)
	user << "<span class='warning'>You need a crowbar to pry this open!</span>"
	return

/obj/structure/barricade/stalker/wood/attackby(obj/item/weapon/W, mob/user, params)
	if(istype(W, /obj/item/weapon/melee/crowbar))
		new /obj/structure/stalker/doski(src)
		var/turf/T = get_turf(src)
		for(var/atom/movable/AM in contents)
			AM.loc = T
		user.visible_message("[user] pries \the [src] open.", \
							 "<span class='notice'>You pry open \the [src].</span>", \
							 "<span class='italics'>You hear splitting wood.</span>")
		playsound(src.loc, 'sound/items/Deconstruct.ogg', 75, 1)
		qdel(src)
	else if(istype(W, /obj/item/weapon/melee/wrench/dmg))
		new /obj/item/stack/sheet/mineral/wood(src)
		var/turf/T = get_turf(src)
		for(var/atom/movable/AM in contents)
			AM.loc = T
		user.visible_message("[user] pries \the [src] open.", \
							 "<span class='notice'>You pry open \the [src].</span>", \
							 "<span class='italics'>You hear splitting wood.</span>")
		playsound(src.loc, 'sound/items/Deconstruct.ogg', 75, 1)
		qdel(src)
	else
		return attack_hand(user)

/obj/structure/barricade/stalker/box
	name = "wooden box"
	desc = "Заколоченный дерев&#255;нный &#255;щик. Кто знает, что может быть внутри."
	icon = 'icons/stalker/decor.dmi'
	icon_state = "yashik"
	debriss_type = /obj/structure/stalker/doski/doski2
	anchored = 1
	proj_pass_rate = 0
	health = 30
	maxhealth = 30
	lootenable = 1
	doubleloot = 1
	loot = list(/obj/item/weapon/reagent_containers/food/snacks/stalker/konserva = 55,
					/obj/item/weapon/reagent_containers/food/snacks/stalker/konserva/shproti = 40,
					/obj/item/weapon/reagent_containers/food/snacks/stalker/konserva/soup = 50,
					/obj/item/weapon/reagent_containers/food/snacks/stalker/kolbasa = 60,
					/obj/item/weapon/reagent_containers/food/snacks/stalker/baton = 75,
					/obj/item/weapon/reagent_containers/food/drinks/bottle/vodka/kazaki = 35,
					/obj/item/stack/medical/bintik = 80,
					/obj/item/weapon/reagent_containers/hypospray/medipen/stalker/zelionka = 10,
					/obj/item/ammo_box/stalker/b12x70 = 40,
					/obj/item/device/radio = 20)

/obj/structure/barricade/stalker/box/re/all
	name = "supply box"
	desc = "Заколоченный &#255;щик. Кто знает, что может быть внутри."
	icon = 'icons/stalker/some_stuff/crates.dmi'
	icon_state = "secure_crate_strapped"
	debriss_type = /obj/structure/stalker/doski/doski2
	anchored = 1
	proj_pass_rate = 0
	health = 30
	maxhealth = 30
	lootenable = 1
	doubleloot = 1
	loot = list(/obj/item/weapon/storage/box/MRE = 60,
					/obj/item/weapon/storage/box/metro/ifak = 25,
					/obj/item/weapon/storage/box/metro/pistol_44_mag_small = 55,
					/obj/item/weapon/storage/box/metro/rifle_545x39_small = 5,
					/obj/item/weapon/reagent_containers/pill/stalker/morphine_plus = 45,
					/obj/item/weapon/reagent_containers/pill/stalker/trava/green = 70,
					/obj/item/weapon/reagent_containers/pill/stalker/trava/red = 50,
					/obj/item/weapon/reagent_containers/pill/stalker/trava/blue = 65,
					/obj/item/weapon/metro/loot/craft/bluepowder = 80,
					/obj/item/weapon/metro/loot/craft/redpowder = 50,
					/obj/item/weapon/metro/loot/craft/yellowpowder = 20,
					/obj/item/weapon/metro/loot/craft/spirt = 40,
					/obj/item/weapon/metro/loot/craft/tryapka = 60,
					/obj/item/stack/medical/bintik = 55,
					/obj/item/stack/medical/thread = 80,
					/obj/item/device/repair_kit/gun = 20,
					/obj/item/weapon/metro/loot/craft/barrel = 30,
					/obj/item/weapon/metro/loot/craft/tape = 70,
					/obj/item/weapon/metro/loot/craft/glue = 56,
					/obj/item/weapon/metro/loot/craft/metall = 74,
					/obj/item/weapon/metro/loot/craft/knifeblade = 70,
					/obj/item/weapon/metro/loot/craft/handle = 56,
					/obj/item/weapon/reagent_containers/pill/stalker/speed = 60,
					/obj/item/weapon/metro/loot/craft/upgrader/health_and_armor = 30,
					/obj/item/weapon/metro/loot/craft/upgrader/damage = 54,
					/obj/item/weapon/metro/loot/craft/upgrader/condition_and_accuracy = 43)

/obj/structure/barricade/stalker/box/re/ammo
	name = "ammo supply box"
	desc = "Заколоченный &#255;щик. Кто знает, что может быть внутри."
	icon = 'icons/stalker/some_stuff/crates.dmi'
	icon_state = "case_double"
	debriss_type = /obj/structure/stalker/doski/doski2
	anchored = 1
	proj_pass_rate = 0
	health = 30
	maxhealth = 30
	lootenable = 1
	doubleloot = 1
	loot = list(/obj/item/weapon/storage/box/metro/pistol_44_mag = 80,
					/obj/item/weapon/storage/box/metro/pistol_44_mag_small = 70,
					/obj/item/weapon/storage/box/metro/pistol_44_mag_fmj = 65,
					/obj/item/weapon/storage/box/metro/rifle_545x39 = 20,
					/obj/item/weapon/storage/box/metro/rifle_545x39_small = 35,
					/obj/item/weapon/storage/box/metro/rifle_545x39ap = 5,
					/obj/item/weapon/storage/box/metro/rifle_545x39ap_small = 35,
					/obj/item/weapon/storage/box/metro/shotgun_b12x70 = 30,
					/obj/item/weapon/storage/box/metro/shotgun_b12x70_small = 35,
					/obj/item/weapon/storage/box/metro/shotgun_b12x70p = 20,
					/obj/item/weapon/metro/loot/craft/whitepowder = 65,
					/obj/item/weapon/metro/loot/craft/yellowpowder = 70,
					/obj/item/weapon/metro/loot/craft/redpowder = 80,
					/obj/item/weapon/metro/loot/craft/bluepowder = 80,
					/obj/item/device/repair_kit/gun = 40)

/obj/structure/barricade/stalker/box/re/weapons
	name = "weapons supply box"
	desc = "Заколоченный &#255;щик. Кто знает, что может быть внутри."
	icon = 'icons/stalker/some_stuff/crates.dmi'
	icon_state = "chest"
	debriss_type = /obj/structure/stalker/doski/doski2
	anchored = 1
	proj_pass_rate = 0
	health = 30
	maxhealth = 30
	lootenable = 1
	doubleloot = 1
	loot = list(/obj/item/weapon/gun/projectile/automatic/pistol/cora = 80,
					/obj/item/weapon/gun/projectile/automatic/pistol/desert = 65,
					/obj/item/weapon/gun/projectile/shotgun/automatic/combat = 20,
					/obj/item/weapon/gun/projectile/automatic/ak12 = 10,
					/obj/item/weapon/gun/projectile/automatic/scar = 20,
					/obj/item/weapon/gun/projectile/automatic/m16a1 = 15,
					/obj/item/weapon/gun/projectile/revolver/metro/revolver = 80,
					/obj/item/weapon/gun/projectile/revolver/metro/douplet = 70,
					/obj/item/weapon/gun/projectile/shotgun/combatshotgun = 50)

/obj/structure/barricade/stalker/box/re/med
	name = "medicine supply box"
	desc = "Заколоченный &#255;щик. Кто знает, что может быть внутри."
	icon = 'icons/stalker/some_stuff/crates.dmi'
	icon_state = "chest_white"
	debriss_type = /obj/structure/stalker/doski/doski2
	anchored = 1
	proj_pass_rate = 0
	health = 30
	maxhealth = 30
	lootenable = 1
	doubleloot = 1
	loot = list(/obj/item/weapon/storage/box/metro/ifak = 60,
					/obj/item/weapon/reagent_containers/pill/stalker/spray = 30,
					/obj/item/weapon/reagent_containers/pill/stalker/trava/blue = 80,
					/obj/item/weapon/reagent_containers/pill/stalker/trava/red = 70,
					/obj/item/weapon/reagent_containers/pill/stalker/trava/green = 80,
					/obj/item/weapon/reagent_containers/pill/stalker/morphine_plus = 70,
					/obj/item/weapon/metro/loot/craft/spirt = 65,
					/obj/item/weapon/metro/loot/craft/tryapka = 80,
					/obj/item/stack/medical/bintik = 80,
					/obj/item/weapon/reagent_containers/pill/stalker/speed = 40,
					/obj/item/stack/medical/carkit = 60,
					/obj/item/stack/medical/grizzly = 45,
					/obj/item/stack/medical/thread = 80)


/obj/structure/stalker/blocks
	name = "blocks"
	icon = 'icons/stalker/decor2.dmi'
	desc = "Обычный бетонный блок."
	eng_desc = "Blocks."
	density = 1
	opacity = 1

/obj/structure/stalker/blocks/block1
	icon_state = "block1"

/obj/structure/stalker/blocks/block1/r
	icon_state = "block1r"

/obj/structure/stalker/blocks/block1/m
	icon_state = "block1m"

/obj/structure/stalker/blocks/block1/l
	icon_state = "block1l"

/obj/structure/stalker/blocks/block2
	icon_state = "block4"

/obj/structure/stalker/blocks/block2/r
	icon_state = "block4r"

/obj/structure/stalker/blocks/block2/m
	icon_state = "block4m"

/obj/structure/stalker/blocks/block2/l
	icon_state = "block4l"

/obj/structure/stalker/blocks/block3
	icon_state = "block3"

/obj/structure/stalker/blocks/block3/r
	icon_state = "block3r"

/obj/structure/stalker/blocks/block3/m
	icon_state = "block3m"

/obj/structure/stalker/blocks/block3/l
	icon_state = "block3l"

/obj/structure/stalker/blocks/vanish
	opacity = 0
	pass_flags = LETPASSTHROW
	var/proj_pass_rate = 50

/obj/structure/stalker/blocks/vanish/block1
	icon_state = "block2"

/obj/structure/stalker/blocks/vanish/block1/r
	icon_state = "block2r"

/obj/structure/stalker/blocks/vanish/block1/m
	icon_state = "block2m"

/obj/structure/stalker/blocks/vanish/block1/l
	icon_state = "block2l"

/obj/structure/stalker/blocks/vanish/block2
	icon_state = "block5"

/obj/structure/stalker/blocks/vanish/block2/r
	icon_state = "block5r"

/obj/structure/stalker/blocks/vanish/block2/m
	icon_state = "block5m"

/obj/structure/stalker/blocks/vanish/block2/l
	icon_state = "block5l"

/obj/structure/stalker/blocks/vanish/CanPass(atom/movable/mover, turf/target, height=0)//So bullets will fly over and stuff.
	if(height==0)
		return 1
	if(istype(mover, /obj/item/projectile))
		if(!anchored)
			return 1
		var/obj/item/projectile/proj = mover
		if(proj.firer && Adjacent(proj.firer))
			return 1
		if(prob(proj_pass_rate))
			return 1
		return 0
	else
		return 0

/obj/structure/stalker/blocks/vanish/pipe
	name = "Pipe"
	desc = "Громадна&#255; железобетонна&#255; труба."
	eng_desc = "Big pipe."
	icon = 'icons/stalker/decor2.dmi'
	icon_state = "truba1"

/obj/structure/stalker/blocks/vanish/pipe/pipe2
	icon_state = "truba2"
	name = "Pipe"

/obj/structure/stalker/blocks/vanish/pipe/pipe3
	icon_state = "truba3"
	name = "Pipe"

/obj/structure/stalker/blocks/vanish/shlagbaum1
	icon = 'icons/stalker/decor.dmi'
	icon_state = "shlagbaum1"

/obj/structure/stalker/blocks/vanish/shlagbaum1/shlagbaum2
	icon_state = "shlagbaum2"

/obj/structure/stalker/blocks/vanish/sandbags
	name = "sandbags"
	desc = "Обычна&#255; насыпь из мешков с песком"
	eng_desc = "Sangbags."
	icon = 'icons/stalker/structure/sandbags.dmi'
	icon_state = "sandbag"

/obj/structure/stalker/blocks/vanish/sandbags/CanPass(atom/movable/mover, turf/target, height=0)
	if(istype(mover, /obj/item/projectile))
		if(!anchored)
			return 1
		var/obj/item/projectile/proj = mover
		if(proj.firer && Adjacent(proj.firer))
			return 1
		if(prob(proj_pass_rate))
			return 1
		return 0
	else
		if(dir == SOUTHWEST || dir == SOUTHEAST || dir == NORTHWEST || dir == NORTHEAST)
			return 0
		if(get_dir(loc, target) == dir)
			return !density
		else
			return 1

/obj/structure/stalker/blocks/vanish/sandbags/CheckExit(atom/movable/O as mob|obj, target)
	if(istype(O, /obj/item/projectile))
		if(!anchored)
			return 1
		var/obj/item/projectile/proj = O
		if(proj.firer && Adjacent(proj.firer))
			return 1
		if(prob(proj_pass_rate))
			return 1
		return 0
	else
		if(get_dir(O.loc, target) == dir)
			return 0
		return 1