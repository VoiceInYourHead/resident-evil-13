/obj/item/weapon/reagent_containers/pill/stalker
	icon = 'icons/stalker/items.dmi'
	w_class = 2
	possible_transfer_amounts = list()
	volume = 60
	apply_type = PATCH
	apply_method = "apply"

/obj/item/weapon/reagent_containers/pill/stalker/aptechka
	name = "kit"
	desc = "Стара&#255; советска&#255; аптечка."

obj/item/weapon/reagent_containers/pill/stalker/afterattack(obj/target, mob/user , proximity)
	return // thanks inheritance again

obj/item/weapon/reagent_containers/pill/stalker/canconsume(mob/eater, mob/user)
	if(!iscarbon(eater))
		return 0
	return 1 // Masks were stopping people from "eating" patches. Thanks, inheritance.

/obj/item/weapon/reagent_containers/pill/stalker/spray
	name = "spray can"
	icon = 'icons/stalker/sPRAY.dmi'
	icon_state = "spray"
	desc = "Обычна&#255; банка лёгкого антисептика, которую каким-то образом превратили в боевой стимулятор."
	eng_desc = "Green spray."
	item_state = "spray"
	list_reagents = list("cryoxadoney" = 6)

/obj/item/weapon/reagent_containers/pill/stalker/morphine_plus
	name = "morphine (+)"
	icon = 'medicine.dmi'
	icon_state = "pervitin"
	desc = "Экспериментальна&#255; помесь двух мощных обезболивающих в одной пилюле. Дл&#255; должного эффекта - советуетс&#255; примен&#255;ть всю пластинку разом."
	eng_desc = "Experimental reagent."
	item_state = "pillpain"
	list_reagents = list("cryoxadone" = 8)

/obj/item/weapon/reagent_containers/pill/stalker/speed
	name = "stimulants (+)"
	icon = 'medicine.dmi'
	icon_state = "big-bottle"
	desc = "Экспериментальна&#255; помесь двух мощных стимул&#255;торов. Дл&#255; должного эффекта - советуетс&#255; примен&#255;ть всю пластинку разом."
	eng_desc = "Experimental reagent."
	item_state = "pillpain"
	list_reagents = list("speedy" = 5)

/obj/item/weapon/reagent_containers/pill/stalker/trava/green
	name = "Green herbage"
	icon = 'omnibus_general_v1a.dmi'
	icon_state = "g"
	desc = "Обыкновенный сорн&#255;к. Такие не редко используютс&#255; при создании различных препаратов"
	eng_desc = "Strange flower"
	item_state = "greenflower"
	list_reagents = list("cryoxadone" = 6)

/obj/item/weapon/reagent_containers/pill/stalker/trava/red
	name = "Red herbage"
	icon = 'omnibus_general_v1a.dmi'
	icon_state = "r"
	desc = "Обыкновенный сорн&#255;к. Такие не редко используютс&#255; при создании различных препаратов"
	eng_desc = "Strange flower"
	item_state = "pinkflower"
	list_reagents = list("cryoxadone" = 5)

/obj/item/weapon/reagent_containers/pill/stalker/trava/blue
	name = "Blue herbage"
	icon = 'omnibus_general_v1a.dmi'
	icon_state = "b"
	desc = "Обыкновенный сорн&#255;к. Такие не редко используютс&#255; при создании различных препаратов"
	eng_desc = "Strange flower"
	item_state = "purpleflower"
	list_reagents = list("pen_acid" = 9, "charcoal" = 30)

/obj/item/weapon/reagent_containers/pill/stalker/trava/gg
	name = "G+G"
	icon = 'omnibus_general_v1a.dmi'
	icon_state = "gg"
	desc = "Комбинаци&#255; двух зелёных трав."
	eng_desc = "Combination of two green herbs."
	item_state = "gg"
	list_reagents = list("cryoxadone" = 16)

/obj/item/weapon/reagent_containers/pill/stalker/trava/rg
	name = "G+R"
	icon = 'omnibus_general_v1a.dmi'
	icon_state = "gr"
	desc = "Комбинаци&#255; зелёной и красной травы."
	eng_desc = "Combination of red and green herbs."
	item_state = "rg"
	list_reagents = list("cryoxadoneb" = 8)

/obj/item/weapon/reagent_containers/pill/stalker/trava/rgb
	name = "R+G+B"
	icon = 'omnibus_general_v1a.dmi'
	icon_state = "grb"
	desc = "Комбинаци&#255; зелёной, красной и синей травы."
	eng_desc = "Combination of red, green and blue herbs."
	item_state = "rgb"
	list_reagents = list("cryoxadoney" = 6)

/obj/item/weapon/reagent_containers/pill/stalker/trava/gb
	name = "G+B"
	icon = 'omnibus_general_v1a.dmi'
	icon_state = "gb"
	desc = "Комбинаци&#255; зелёной и синей травы."
	eng_desc = "Combination of green and blue herbs."
	item_state = "gb"
	list_reagents = list("cryoxadone" = 10, "pen_acid" = 18)

/obj/item/weapon/reagent_containers/pill/stalker/trava/ggg
	name = "G+G+G"
	icon = 'omnibus_general_v1a.dmi'
	icon_state = "ggg"
	desc = "Комбинаци&#255; трёх зелёных трав."
	eng_desc = "Combination of three green herbs."
	item_state = "ggg"
	list_reagents = list("cryoxadoney" = 6)

/obj/structure/sign/gflowerpot
	name = "Flowerpot"
	desc = "Горшок с зелёным растением."
	icon = 'omnibus_general_v1a.dmi'
	icon_state = "g_pot"
	var/ruined = 0
	anchored = 1

/obj/structure/sign/gflowerpot/attack_hand(mob/user)
	if(ruined)
		return
	var/temp_loc = user.loc
	if((user.loc != temp_loc) || ruined )
		return
	visible_message("[user] rips [src] in a single, decisive motion!" )
	playsound(src.loc, 'sound/items/poster_ripped.ogg', 100, 1)
	ruined = 1
	icon_state = "pot"
	name = "ripped flowerpot"
	desc = "Пустой горшок."
	new/obj/item/weapon/reagent_containers/pill/stalker/trava/green(src.loc)

/obj/structure/sign/bflowerpot
	name = "Flowerpot"
	desc = "Горшок с синим растением."
	icon = 'omnibus_general_v1a.dmi'
	icon_state = "b_pot"
	anchored = 1
	var/ruined = 0

/obj/structure/sign/bflowerpot/attack_hand(mob/user)
	if(ruined)
		return
	var/temp_loc = user.loc
	if((user.loc != temp_loc) || ruined )
		return
	visible_message("[user] rips [src] in a single, decisive motion!" )
	playsound(src.loc, 'sound/items/poster_ripped.ogg', 100, 1)
	ruined = 1
	icon_state = "pot"
	name = "ripped flowerpot"
	desc = "Пустой горшок."
	new/obj/item/weapon/reagent_containers/pill/stalker/trava/blue(src.loc)

/obj/structure/sign/rflowerpot
	name = "Flowerpot"
	desc = "Горшок с красным растением."
	icon = 'omnibus_general_v1a.dmi'
	icon_state = "r_pot"
	var/ruined = 0
	anchored = 1

/obj/structure/sign/rflowerpot/attack_hand(mob/user)
	if(ruined)
		return
	var/temp_loc = user.loc
	if((user.loc != temp_loc) || ruined )
		return
	visible_message("[user] rips [src] in a single, decisive motion!" )
	playsound(src.loc, 'sound/items/poster_ripped.ogg', 100, 1)
	ruined = 1
	icon_state = "pot"
	name = "ripped flowerpot"
	desc = "Пустой горшок."
	new/obj/item/weapon/reagent_containers/pill/stalker/trava/red(src.loc)