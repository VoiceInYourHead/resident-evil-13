/obj/item/weapon/melee/knife/tourist
	name = "hunting knife"
	desc = "Дешевый нож дл&#255; разделывани&#255; добычи...И людей."
	eng_desc = "Cheap knife but good enough for a tourist."
	icon = 'icons/stalker/lohweb/weapons.dmi'
	icon_state = "hunting"
	flags = CONDUCT
	slot_flags = SLOT_BELT
	force = 25
	throwforce = 15
	w_class = 3
	hitsound = 'sound/weapons/bladeslice.ogg'
	attack_verb = list("attacked", "slashed", "stabbed", "sliced", "torn", "ripped", "diced", "cut")
	sharpness = IS_SHARP_ACCURATE

/obj/item/weapon/melee/knife/bayonet
	name = "bayonet"
	desc = "Опасный, но плохой в разделке туш мутантов штык-нож."
	eng_desc = "Dangerous, but bad at butchering mutants bayonet."
	icon = 'icons/stalker/weapons.dmi'
	icon_state = "bayonet"
	flags = CONDUCT
	slot_flags = SLOT_BELT
	force = 70
	throwforce = 50
	w_class = 3
	hitsound = 'sound/weapons/bladeslice.ogg'
	attack_verb = list("attacked", "stabbed", "torn", "ripped")
	sharpness = IS_SHARP_ACCURATE

/obj/item/weapon/melee/knife/battle_axe
	name = "Battle Sword"
	desc = "Меч с зазубринами. Отлично...Рубит?"
	icon = 'omnibus_weapons_icons_v1a.dmi'
	icon_state = "kukri"
	item_state = "bayonet"
	flags = CONDUCT
	slot_flags = SLOT_BELT
	force = 90
	throwforce = 50
	w_class = 2
	hitsound = 'sound/weapons/bladeslice.ogg'
	attack_verb = list("attacked", "stabbed", "torn", "ripped")
	sharpness = IS_SHARP_ACCURATE

/obj/item/weapon/melee/knife/battle_axe/dmg
	name = "Battle Sword"
	desc = "Меч с зазубринами. Отлично...Рубит?"
	icon = 'omnibus_weapons_icons_v1a.dmi'
	icon_state = "kukri_dmg"
	item_state = "bayonet"
	flags = CONDUCT
	slot_flags = SLOT_BELT
	force = 110
	throwforce = 70
	w_class = 2
	hitsound = 'sound/weapons/bladeslice.ogg'
	attack_verb = list("attacked", "stabbed", "torn", "ripped")
	sharpness = IS_SHARP_ACCURATE

/obj/item/weapon/melee/knife/zatochka
	name = "Makeshift knife"
	desc = "Поделка из говна и палок. Бьёт больно, но хватит на пару ударов."
	icon = 'icons/stalker/metro-2/weapons.dmi'
	icon_state = "hc_knife"
	flags = CONDUCT
	slot_flags = SLOT_BELT
	force = 1
	throwforce = 300
	w_class = 2
	hitsound = 'sound/weapons/bladeslice.ogg'
	attack_verb = list("attacked", "slashed", "stabbed", "sliced", "torn", "ripped", "diced", "cut")
	sharpness = IS_SHARP_ACCURATE

/obj/item/weapon/melee/knife/zatochka/throw_impact(atom/target,mob/thrower)
	..(target,thrower)
	smash(target,thrower,1)
	return

/obj/item/weapon/melee/knife/zatochka/proc/smash(mob/living/target, mob/living/user, ranged = 0)
	//Creates a shattering noise and replaces the bottle with a broken_bottle
	var/new_location = get_turf(loc)
	var/obj/item/weapon/melee/knife/zatochka_broken/Z = new /obj/item/weapon/melee/knife/zatochka_broken(new_location)
	if(ranged)
		Z.loc = new_location
	else
		user.drop_item()
		user.put_in_active_hand(Z)

	qdel(src)

/obj/item/weapon/melee/knife/zatochka_broken
	name = "Broken makeshift knife"
	desc = "Этим уже не повоюешь."
	icon = 'icons/stalker/metro-2/weapons.dmi'
	icon_state = "knife_broken"
	flags = CONDUCT
	slot_flags = SLOT_BELT
	force = 1
	throwforce = 1
	w_class = 2
	hitsound = 'sound/weapons/bladeslice.ogg'
	attack_verb = list("attacked", "slashed", "stabbed", "sliced", "torn", "ripped", "diced", "cut")
	sharpness = IS_SHARP_ACCURATE

/obj/item/weapon/melee/wrench
	name = "wrench"
	desc = "A wrench with common uses. Can be found in your hand."
	icon = 'tools.dmi'
	icon_state = "wrench"
	flags = CONDUCT
	slot_flags = SLOT_BELT
	force = 36
	throwforce = 12
	w_class = 2

/obj/item/weapon/melee/wrench/dmg
	name = "wrench"
	desc = "A wrench with common uses. Can be found in your hand."
	icon = 'tools.dmi'
	icon_state = "big-wrench"
	flags = CONDUCT
	slot_flags = SLOT_BELT
	force = 50
	throwforce = 16
	w_class = 3

/obj/item/weapon/melee/crowbar
	name = "crowbar"
	desc = "A small crowbar. This handy tool is useful for lots of things, such as prying floor tiles or opening unpowered doors."
	icon = 'icons/stalker/lohweb/items.dmi'
	icon_state = "crowbar"
	flags = CONDUCT
	slot_flags = SLOT_BELT
	force = 34
	throwforce = 12
	w_class = 3