/obj/item/weapon/stalker/loot
	icon = 'icons/stalker/loot.dmi'

/obj/item/weapon/stalker/loot/dog_tail
	name = "dog tail"
	desc = "Устойчивость кожи мутировавших собак к химическому и электрическому воздействию давно обратила на себ&#255; внимание учёных. Из-за отсутстви&#255; жировых отложений хвосты собак наиболее подход&#255;т дл&#255; лабораторных исследований"
	eng_desc = "The resistance of mutant dogs' skin to electricity and chemical agents has long been the subject of scientific attention. Since the animal's tail is almost completely free of fat tissue, it is especially suited for lab experimentation."
	icon_state = "pes"

/obj/item/weapon/stalker/loot/flesh_eye
	name = "flesh eye"
	desc = "После мутаций увеличенный глаз Плоти приобрёл р&#255;д свойств, самым полезным из которых &#255;вл&#255;етс&#255; способность к регенерации хрусталика. Детали этого механизма точно не известны, но это открытие обещает революцию в медицине. Вы можете внести свой вклад в науку, продав глаз плоти учёным дл&#255; исследований."
	eng_desc = "The mutated, swollen eye of the Flesh acquired a number of strange characteristics, the most useful of which is the ability to regenerate the lens. The specifics of the process are unknown, although such discovery would most certainly revolutionize modern medicine. You can play your part in scientific development by selling Flesh eyes to scientists for their experiments."
	icon_state = "plot"

/obj/item/weapon/stalker/loot/boar_leg
	name = "boar's hoof"
	desc = "Копыто кабана используетс&#255; учёными дл&#255; изготовлени&#255; р&#255;да лекарств, самым распространённым из которых &#255;вл&#255;етс&#255; противорадиационна&#255; сыворотка."
	eng_desc = "The hoof of a wild boar is used by scientists for manufacturing of some medicines, the most widespread of which are anti radiation drugs."
	icon_state = "kaban"

/obj/item/weapon/stalker/loot/snork_leg
	name = "snork leg"
	desc = "The resistance of mutant dogs' skin to electricity and chemical agents has long been the subject of scientific attention. Since the animal's tail is almost completely free of fat tissue, it is especially suited for lab experimentation."
	eng_desc = "Snork feet contain a huge number of highly elastic tendons. This explains the beast's ability to make those unbelievably long jumps."
	icon_state = "snork_leg"

/obj/item/weapon/stalker/loot/pseudo_tail
	name = "pseudo-dog tail"
	desc = "Устойчивость кожи мутировавших собак к химическому и электрическому воздействию давно обратила на себ&#255; внимание учёных. Из-за отсутстви&#255; жировых отложений хвосты собак наиболее подход&#255;т дл&#255; лабораторных исследований."
	icon_state = "psevdopes"

/obj/item/weapon/stalker/loot/bloodsucker
	name = "bloodsucker tentacles"
	desc = "Щупальца Кровососа содержат особую железу: она выдел&#255;ет фермент, раствор&#255;ющий кожу жертвы, и одновременно преп&#255;тствующий сворачиванию крови. Если учёным удастьс&#255; определить структуру фермента, то это поможет осуществить прорыв в медицине."
	icon_state = "bloodsucker"

/obj/structure/table/crafting
	name = "crafting table"
	desc = "A square piece of metal standing on four metal legs. It can not move."
	icon = 'icons/obj/smooth_structures/crafting.dmi'
	icon_state = "btable2"
	var/brightness = 1
	deconstructable = 0
	smooth = SMOOTH_FALSE

/obj/item/weapon/stalker/loot/controller_brain
	name = "controller brain"
	icon_state = "controller_brain"

/obj/item/weapon/metro/loot/craft
	icon = 'icons/stalker/drugs.dmi'

/obj/item/weapon/metro/loot/craft/bluepowder
	name = "small blue powder bottle"
	desc = "Небольший размеров банка, с порохом внутри. Используетс&#255; дл&#255; создани&#255; боеприпасов."
	icon_state = "bluepowder"
	w_class = 1

/obj/item/weapon/metro/loot/craft/redpowder
	name = "big red powder bottle"
	desc = "Банка пороха. Используетс&#255; дл&#255; создани&#255; боеприпасов."
	icon_state = "redpowder"
	w_class = 1

/obj/item/weapon/metro/loot/craft/yellowpowder
	name = "yellow powder bottle"
	desc = "Банка пороха. Используетс&#255; дл&#255; создани&#255; боеприпасов."
	icon_state = "yellowpowder"
	w_class = 1

/obj/item/weapon/metro/loot/craft/whitepowder
	name = "white powder bottle"
	desc = "Банка пороха. Используетс&#255; дл&#255; создани&#255; боеприпасов."
	icon_state = "whitepowder"
	w_class = 1

/obj/item/weapon/metro/loot/antidot
	name = "antidot injector"
	desc = "Капсула с образцом антидота."
	icon = 'icons/stalker/lohweb/personal.dmi'
	icon_state = "antidot"
	w_class = 1

/obj/item/weapon/metro/loot/saleble
	icon = 'icons/stalker/lohweb/items.dmi'

/obj/item/weapon/metro/loot/saleble/map
	name = "map"
	desc = "Немного пожелтевша&#255; бумажка, с накинутой поверх картой. Такие часто используютс&#255; сталкерами и высоко цен&#255;тс&#255; у торговцев."
	icon_state = "map"
	w_class = 1

/obj/item/weapon/metro/loot/saleble/plasma
	name = "big monitor"
	desc = "Плазма. Хорошо сохранилась. Мало-полезна в обычном быту, однако инженеры с крупных станций, или, к примеру, Кшатрии - такой презент оценили бы."
	icon_state = "plasmatv"
	w_class = 3

/obj/item/weapon/metro/loot/saleble/map2
	name = "advanced map"
	desc = "Одна из заметок сталкеров с станций Союза. Написана на каком-то непон&#255;тном &#255;зыке. Вполне возможно, знающий человек сможет рассказать о её содержимом больше. Стоит така&#255; штука точно не мало."
	icon = 'icons/stalker/lohweb/personal.dmi'
	icon_state = "oldmap"
	w_class = 1

/obj/item/weapon/metro/loot/saleble/metro_2_code
	name = "secret documents"
	desc = "Небольших размеров папка, с грифом СЕКРЕТНО на обложке. Наверн&#255;ка стоит целого состояни&#255;."
	icon = 'icons/stalker/lohweb/personal.dmi'
	icon_state = "diary0"
	w_class = 1

/obj/item/weapon/metro/loot/saleble/medved
	name = "plush bear"
	desc = "Плюшевый медведь"
	icon = 'icons/stalker/lohweb/personal.dmi'
	icon_state = "teddybear"
	w_class = 1

/obj/item/weapon/metro/loot/saleble/boombox
	name = "old boombox"
	desc = "Потрёпанный бумбокс. Батареек внутри нет. Сложно сказать, можно ли кому-то такой сбагрить."
	icon = 'icons/stalker/some_stuff/boombox.dmi'
	icon_state = "boombox"
	w_class = 2

/obj/item/weapon/metro/loot/saleble/wristwatch
	name = "wrist watch"
	desc = "Наручные часы. Что удивительно - всё ещё идут."
	icon = 'icons/stalker/lohweb/personal.dmi'
	icon_state = "wristwatch"
	w_class = 1

/obj/item/weapon/metro/loot/saleble/radio
	name = "old radio"
	desc = "Ржавое радио. Не работает. Можно пустить на запчасти или продать. Пускай, скорее всего, и не за большую цену."
	icon = 'icons/stalker/lohweb/device.dmi'
	icon_state = "radio"
	w_class = 2

/obj/item/weapon/metro/loot/saleble/parts
	name = "details"
	desc = "Ничейные запчасти."
	icon_state = "godport"
	w_class = 1

/obj/item/weapon/metro/loot/saleble/parts2
	name = "details"
	desc = "Ничейные запчасти."
	icon_state = "neuroimplant"
	w_class = 1

/obj/item/weapon/metro/loot/saleble/parts3
	name = "details"
	desc = "Ничейные запчасти."
	icon_state = "harddisk"
	w_class = 1

/obj/item/weapon/metro/loot/saleble/plastine
	name = "vinil plastine"
	desc = "Виниловая пластинка. Раритет в метро."
	icon = 'icons/stalker/some_stuff/Other.dmi'
	icon_state = "vinil"
	w_class = 1

/obj/item/weapon/metro/loot/saleble/vinilplayer
	name = "small vinil player"
	desc = "Проигрыватель дл&#255; виниловых пластинок. Увесистый."
	icon = 'icons/stalker/some_stuff/Other.dmi'
	icon_state = "vinilplayer"
	w_class = 3


/obj/item/weapon/metro/loot/craft/spirt
	name = "spirt bottle"
	desc = "Баночка спирт&#255;ги. Можно использовать дл&#255; обеззараживани&#255; различных реагентов и предметов(к примеру тр&#255;пок)."
	icon = 'icons/stalker/lohweb/items.dmi'
	icon_state = "ointment"
	w_class = 1

/obj/item/weapon/metro/loot/craft/tryapka
	name = "dirty cloth"
	desc = "Гр&#255;зна&#255; тр&#255;пка. Если обработать спиртом - может выйти неплохой бинт."
	icon = 'icons/stalker/lohweb/items.dmi'
	icon_state = "clothbandaid"
	w_class = 1

/obj/item/weapon/metro/loot/craft/metall
	name = "scrap metall"
	desc = "Куча металлолома. Можно пустить на запчасти или детали к чему-либо."
	icon = 'icons/stalker/lohweb/items.dmi'
	icon_state = "scrap2"
	w_class = 1

/obj/item/weapon/metro/loot/craft/powder
	name = "black powder"
	desc = "Горстка пороха. Используетс&#255; дл&#255; создани&#255; боеприпасов."
	icon = 'icons/stalker/lohweb/newammo.dmi'
	icon_state = "gp10"
	w_class = 1

/obj/item/weapon/metro/loot/craft/barrel
	name = "barrel"
	desc = "Металлическая труба."
	icon = 'icons/stalker/metro-2/weapon_assembly.dmi'
	icon_state = "barrel"
	w_class = 1

/obj/item/weapon/metro/loot/craft/axe
	name = "axe blade"
	desc = "Лезвие топора."
	icon = 'icons/stalker/lohweb/weapons.dmi'
	icon_state = "lezvie"
	w_class = 1

/obj/item/weapon/metro/loot/craft/tape
	name = "ducttape"
	desc = "Изолента. Хороша в ситуациЯх, когда надо соединить несколько деталей в нечто целостное."
	icon = 'icons/stalker/metro-2/craftlootable.dmi'
	icon_state = "ducttape"
	w_class = 1

/obj/item/weapon/metro/loot/craft/glue
	name = "glue"
	desc = "Обычный клей. Может быть на что-то и сгодитсЯ."
	icon = 'icons/stalker/metro-2/craftlootable.dmi'
	icon_state = "glue"
	w_class = 1

/obj/item/weapon/metro/loot/craft/knifeblade
	name = "knife blade"
	desc = "Основа для лезвиЯ ножа."
	icon = 'icons/stalker/metro-2/ph_craft.dmi'
	icon_state = "knifeblade0"
	w_class = 1

/obj/item/weapon/metro/loot/craft/handle
	name = "wood handle"
	desc = "ДеревЯннаЯ ручка. СгодитсЯ для разного."
	icon = 'icons/stalker/metro-2/ph_craft.dmi'
	icon_state = "bladeruchka"
	w_class = 1

/obj/item/weapon/metro/loot/craft/upgrader
	name = "upgrade blueprint"
	desc = "Компактный девайс, содержащий в себе с десЯток чертежей."
	icon = 'icons/stalker/lohweb/card.dmi'
	icon_state = "id"
	w_class = 1

/obj/item/weapon/metro/loot/craft/upgrader/health_and_armor
	name = "white upgrade blueprint"
	desc = "Компактный девайс, содержащий в себе с десЯток чертежей."
	icon = 'icons/stalker/lohweb/card.dmi'
	icon_state = "key_med"
	w_class = 1

/obj/item/weapon/metro/loot/craft/upgrader/damage
	name = "red upgrade blueprint"
	desc = "Компактный девайс, содержащий в себе с десЯток чертежей."
	icon = 'icons/stalker/lohweb/card.dmi'
	icon_state = "key_sec"
	w_class = 1

/obj/item/weapon/metro/loot/craft/upgrader/condition_and_accuracy
	name = "black upgrade blueprint"
	desc = "Компактный девайс, содержащий в себе с десЯток чертежей."
	icon = 'icons/stalker/lohweb/card.dmi'
	icon_state = "key_tech"
	w_class = 1

/obj/item/weapon/metro/loot/craft/chain
	name = "chain"
	desc = "Цепь. Можно использовать как в мирных, так и не очень мирных целЯх."
	icon = 'icons/stalker/metro-2/weapons.dmi'
	icon_state = "chainn"
	w_class = 1