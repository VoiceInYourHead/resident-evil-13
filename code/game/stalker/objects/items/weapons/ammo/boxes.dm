/obj/item/ammo_box/stalker
	name = "ammo box (5.45x39mm)"
	desc = "It shouldn't be here"
	icon = 'icons/stalker/ammo.dmi'
	icon_state = "545x39"
	multiple_sprites = 2

/obj/item/ammo_box/stalker/b545
	name = "handful of rifle ammo"
	desc = "Обычный стандартный патрон, простой сердечник, оболочечна&#255; пул&#255; (5.45x39мм)."
	ammo_type = /obj/item/ammo_casing/c545
	icon_state = "545x39"
	max_ammo = 9
	multiple_sprites = 1

/obj/item/ammo_box/stalker/b545ap
	name = "handful of rifle ammo (AP)"
	desc = "Бронебойный патрон дл&#255; автоматических винтовок стран Варшавского договора. Крайне редок, также используетс&#255; в качестве валюты. (5.45x39мм AP)."
	ammo_type = /obj/item/ammo_casing/c545/AP
	icon_state = "545x39ap"
	max_ammo = 9
	multiple_sprites = 1

/obj/item/ammo_box/stalker/b9x18
	name = "ammo box (9x18mm)"
	desc = "Патрон с оболочечной пулей. Обладает хорошим останавливающем действием, однако малоэффективен против бронированных целей (9x18мм)."
	ammo_type = /obj/item/ammo_casing/c9x18
	icon_state = "9x18"
	max_ammo = 50
	multiple_sprites = 2

/obj/item/ammo_box/stalker/b9x18P
	name = "ammo box (9x18mm +P+)"
	desc = "Патрон с улучшенными баллистическими свойствами, выраженный бронебойный эффект (9x18мм +P+)."
	ammo_type = /obj/item/ammo_casing/c9x18/P
	icon_state = "9x18P"
	max_ammo = 50
	multiple_sprites = 2

/obj/item/ammo_box/stalker/b12x70
	name = "handful of shotgun ammo"
	desc = "Обычный патрон 12 калибра с 6-мм дробью, огромна&#255; убойна&#255; сила (12x70мм Дробь)."
	ammo_type = /obj/item/ammo_casing/shotgun/c12x70
	icon_state = "12x70"
	max_ammo = 8
	multiple_sprites = 1

/obj/item/ammo_box/stalker/b12x70P
	name = "handful of shotgun ammo (AP)"
	desc = "Бронебойные патроны 12-го калибра. Смерть всему, что окажетс&#255; в пределах зоны поражени&#255;."
	ammo_type = /obj/item/ammo_casing/shotgun/c12x70p
	icon_state = "12x70P"
	max_ammo = 8
	multiple_sprites = 1

/obj/item/ammo_box/stalker/b9x19
	name = "ammo box (9x19mm)"
	desc = "Базовый патрон, оболочечна&#255; пул&#255;, слабое бронебойное действие (9x19мм)."
	ammo_type = /obj/item/ammo_casing/c9x19
	icon_state = "9x19"
	max_ammo = 50
	multiple_sprites = 2

/obj/item/ammo_box/stalker/b9x19P
	name = "ammo box (9x19mm PBP)"
	desc = "Мощный патрон с экспансивной пулей и бронебойным сердечником (9x19мм PBP)."
	ammo_type = /obj/item/ammo_casing/c9x19/P
	icon_state = "9x19P"
	max_ammo = 50
	multiple_sprites = 2

/obj/item/ammo_box/stalker/b55645
	name = "handful of rifle ammo"
	desc = "Стандартный натовский патрон, широко известный под бельгийским индексом SS109. Используетс&#255; в таких штурмовых винтовках как М16 и SCAR-H."
	ammo_type = /obj/item/ammo_casing/c556x45
	icon_state = "556x45"
	max_ammo = 15
	multiple_sprites = 1

/obj/item/ammo_box/stalker/b55645ap
	name = "ammo box (5.56x45mm AP)"
	desc = "Патрон с бронебойной пулей дл&#255; штурмовых винтовок М16 и SCAR-H."
	ammo_type = /obj/item/ammo_casing/c556x45/AP
	icon_state = "556x45ap"
	max_ammo = 90
	multiple_sprites = 2

/obj/item/ammo_box/stalker/b762x54
	name = "handful of rifle ammo"
	desc = "Стандартный винтовочный патрон, имеет повышенное останавливающее действие."
	ammo_type = /obj/item/ammo_casing/c762x54
	icon_state = "762x54"
	max_ammo = 9
	multiple_sprites = 1

/obj/item/ammo_box/stalker/bacp45
	name = "ammo box (.45 ACP)"
	desc = "Пистолетный патрон калибра 11,43 мм."
	ammo_type = /obj/item/ammo_casing/acp45
	icon_state = "45_ACP"
	max_ammo = 50
	multiple_sprites = 2

/obj/item/ammo_box/stalker/bacp45ap
	name = "ammo box (.45 ACP Hydroshock)"
	desc = "Экспансивный патрон калибра 11,43 мм., который используетс&#255; в убойных пистолетах."
	ammo_type = /obj/item/ammo_casing/acp45/P
	icon_state = "45_ACP_hydroshock"
	max_ammo = 50
	multiple_sprites = 2

/obj/item/ammo_box/stalker/bmag44
	name = "handful of pistol ammo"
	desc = "Пистолетный патрон калибра .44 Магнум. Возможно, даже слишком мощный."
	ammo_type = /obj/item/ammo_casing/mag44
	icon_state = "44_Mag"
	max_ammo = 15
	multiple_sprites = 1

/obj/item/ammo_box/stalker/bmag44fmj
	name = "handful of pistol ammo (AP)"
	desc = "Пистолетный патрон калибра .44 Магнум, оболоченный. Пробивает навылет любую легкую броню."
	ammo_type = /obj/item/ammo_casing/mag44/FMJ
	icon_state = "44_Mag_FMJ"
	max_ammo = 15
	multiple_sprites = 1

/obj/item/ammo_box/stalker/b762x25
	name = "ammo box (7.62x25mm)"
	desc = "Пистолетный патрон калибра 7.62x25мм, разработанный в прошлом веке дл&#255; советского самозар&#255;дного оружи&#255;. Морально и технически устарел, но еще используетс&#255; в Зоне."
	ammo_type = /obj/item/ammo_casing/c762x25
	icon_state = "762x25"
	max_ammo = 50
	multiple_sprites = 2

/obj/item/ammo_box/stalker/b762x51
	name = "ammo box(7.62x51mm)"
	desc = "An ammo box."
	eng_desc = "An ammo box."
	ammo_type = /obj/item/ammo_casing/c762x51
	icon_state = "762x51"
	max_ammo = 40
	multiple_sprites = 2

/obj/item/ammo_box/stalker/cl762x51
	name = "stripper clip(7.62x51mm)"
	desc = "A stripper clip."
	eng_desc = "A stripper clip."
	ammo_type = /obj/item/ammo_casing/c762x51
	icon_state = "303"
	max_ammo = 5
	multiple_sprites = 1