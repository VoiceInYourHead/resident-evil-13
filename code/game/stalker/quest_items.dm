/obj/item/stalker
	icon = 'icons/stalker/quest.dmi'
	desc = "Quest items."
	w_class = 1

/obj/item/stalker/docs
	icon_state = "docs0"
	name = "Documents"
	desc = "Старые документы."
	eng_desc = "Old documents. Trader may pay a big price for this."

/obj/item/stalker/usb
	icon_state = "USB"
	name = "USB"
	desc = "СинЯЯ флешка на 2 Гб."
	eng_desc = "Blue USB 2 Gb. Trader may pay a big price for this."

/obj/item/stalker/blue_box
	icon_state = "blue_box"
	name = "Box"
	desc = "Закрытый синий Ящик. В нем должно быть что-то ценное."
	eng_desc = "Closed blue blox. Trader may pay a big price for this."

/obj/item/stalker/microsd
	icon_state = "microSD"
	name = "microSD"
	desc = "МаленькаЯ SD карта на 128 Мб."
	eng_desc = "MicroSD 128 Mb. Trader may pay a big price for this."