/obj/item/weapon/card/id/metro
	name = "civilian identification card"
	desc = "Карто-подобный паспорт, используемый в метро практически повсеместно. На нём также виднеетс&#255; пункт с местом прописки. По всей видимости - это гражданский с одной из независимых станций."
	icon = 'icons/stalker/lohweb/items.dmi'
	icon_state = "documents"
	item_state = "card-id"
	registered_name = "Civilian"
	slot_flags = SLOT_ID
	assignment = "Civilian"

/obj/item/weapon/card/id/metro/trc
	name = "D.E.S.T.R.O. employee identification card"
	desc = "Карто-подобный паспорт."
	icon = 'icons/stalker/lohweb/items.dmi'
	icon_state = "pdocuments"
	item_state = "card-id"
	registered_name = "TRC dweller"
	slot_flags = SLOT_ID
	assignment = "Employee"

/obj/item/weapon/card/id/metro/stalker
	name = "ERT dogtag"
	desc = "Маленький металлический жетон."
	icon = 'icons/Unsorted/mistakes (1).dmi'
	icon_state = "dogtag"
	item_state = "card-id"
	registered_name = "ERT Operative"
	slot_flags = SLOT_ID
	assignment = "ERT Operative"

/obj/item/weapon/card/id/metro/key
	name = "gold key"
	desc = "Маленький золотой ключик."
	icon = 'icons/stalker/some_stuff/items.dmi'
	icon_state = "key3"
	item_state = "card-id"
	registered_name = null
	slot_flags = SLOT_ID
	assignment = null

/obj/item/weapon/card/id/metro/idblue
	name = "blue id-card"
	desc = "Син&#255;&#255; айди-карта."
	icon = 'icons/stalker/some_stuff/quest.dmi'
	icon_state = "id_card_blue"
	item_state = "card-id"
	slot_flags = SLOT_ID
	access = list(access_security)

/obj/item/weapon/card/id/metro/idyellow
	name = "yellow id-card"
	desc = "Жёлта&#255; айди-карта."
	icon = 'icons/stalker/some_stuff/quest.dmi'
	icon_state = "id_card_yellow"
	item_state = "card-id"
	slot_flags = SLOT_ID
	access = list(access_emergency_storage)

/obj/item/weapon/card/id/metro/idred
	name = "red id-card"
	desc = "Красна&#255; айди-карта."
	icon = 'icons/stalker/some_stuff/quest.dmi'
	icon_state = "id_card_red"
	item_state = "card-id"
	slot_flags = SLOT_ID
	access = list(access_research)

/obj/item/weapon/card/id/metro/tank
	name = "fuel tank"
	desc = "Канистра топлива."
	icon = 'icons/stalker/some_stuff/tank.dmi'
	icon_state = "canister"
	item_state = "card-id"
	slot_flags = SLOT_ID
	access = list(20)

/obj/item/weapon/card/id/metro/hand
	name = "hand"
	desc = "Полу-сгнившая рука."
	icon = 'icons/stalker/loot.dmi'
	icon_state = "snork"
	item_state = "card-id"
	slot_flags = SLOT_ID
	access = list(50)