/obj/item/weapon/implant/weapons_auth
	name = "firearms authentication implant"
	desc = "Lets you shoot your guns"
	icon_state = "auth"
	origin_tech = "materials=2;magnets=2;programming=2;biotech=5;syndicate=5"
	activated = 0

/obj/item/weapon/implant/weapons_auth/get_data()
	var/dat = {"<b>Implant Specifications:</b><BR>
				<b>Name:</b> Firearms Authentication Implant<BR>
				<b>Life:</b> 4 hours after death of host<BR>
				<b>Implant Details:</b> <BR>
				<b>Function:</b> Allows operation of implant-locked weaponry, preventing equipment from falling into enemy hands."}
	return dat


/obj/item/weapon/implant/survivor
	name = "survivor perk"
	desc = "Возвращает к жизни за бесплатно."
	icon_state = "survivor"
	origin_tech = "materials=2;biotech=4;combat=3;syndicate=4"
	uses = 2
	layer = 22

/obj/item/weapon/implant/survivor/get_data()
	var/dat = {"<b>Implant Specifications:</b><BR>
				<b>Name:</b> Cybersun Industries Adrenaline Implant<BR>
				<b>Life:</b> Five days.<BR>
				<b>Important Notes:</b> <font color='red'>Illegal</font><BR>
				<HR>
				<b>Implant Details:</b> Subjects injected with implant can activate an injection of medical cocktails.<BR>
				<b>Function:</b> Removes stuns, increases speed, and has a mild healing effect.<BR>
				<b>Integrity:</b> Implant can only be used two times before reserves are depleted."}
	return dat

/obj/item/weapon/implant/survivor/activate()
	if(uses < 1)	return 0
	uses--
	imp_in << "<span class='notice'>You feel a sudden surge of energy!</span>"
	imp_in.SetStunned(0)
	imp_in.AdjustWeakened(-2)
	imp_in.SetParalysis(0)
	imp_in.adjustStaminaLoss(-70)
	imp_in.lying = 0
	imp_in.update_canmove()

	imp_in.reagents.add_reagent("cryoxadoney", 6)

/obj/item/weapon/implant/speedster
	name = "fast walker perk"
	desc = "Почувствуй себЯ Флешем."
	icon_state = "fast"
	origin_tech = "materials=2;biotech=4;combat=3;syndicate=4"
	uses = 3
	layer = 22

/obj/item/weapon/implant/speedster/get_data()
	var/dat = {"<b>Implant Specifications:</b><BR>
				<b>Name:</b> Cybersun Industries Speedster Implant<BR>
				<b>Life:</b> Five days.<BR>
				<b>Important Notes:</b> <font color='red'>Illegal</font><BR>
				<HR>
				<b>Implant Details:</b> Subjects injected with implant can activate an injection of medical cocktails.<BR>
				<b>Function:</b> Increases speed for small time period.<BR>
				<b>Integrity:</b> Implant can only be used three times before reserves are depleted."}
	return dat

/obj/item/weapon/implant/speedster/activate()
	if(uses < 1)	return 0
	uses--
	imp_in << "<span class='notice'>You feel a sudden surge of energy!</span>"

	imp_in.reagents.add_reagent("speedy", 10)

/obj/item/weapon/implant/med
	name = "med-injection perk"
	desc = "Возвращает к жизни за бесплатно."
	icon_state = "medicine"
	origin_tech = "materials=2;biotech=4;combat=3;syndicate=4"
	uses = 3
	layer = 22

/obj/item/weapon/implant/med/get_data()
	var/dat = {"<b>Implant Specifications:</b><BR>
				<b>Name:</b> Cybersun Industries Medicine Implant<BR>
				<b>Life:</b> Five days.<BR>
				<b>Important Notes:</b> <font color='red'>Illegal</font><BR>
				<HR>
				<b>Implant Details:</b> Subjects injected with implant can activate an injection of medical cocktails.<BR>
				<b>Function:</b> Removes stuns, increases speed, and has a mild healing effect.<BR>
				<b>Integrity:</b> Implant can only be used threeo times before reserves are depleted."}
	return dat

/obj/item/weapon/implant/med/activate()
	if(uses < 1)	return 0
	uses--
	imp_in << "<span class='notice'>You feel a sudden surge of energy!</span>"
	imp_in.SetStunned(0)
	imp_in.AdjustWeakened(-2)
	imp_in.SetParalysis(0)
	imp_in.adjustStaminaLoss(-50)
	imp_in.lying = 0
	imp_in.update_canmove()

	imp_in.reagents.add_reagent("cryoxadoneb", 8)


/obj/item/weapon/implant/emp
	name = "emp implant"
	desc = "Triggers an EMP."
	icon_state = "emp"
	origin_tech = "materials=2;biotech=3;magnets=4;syndicate=4"
	uses = 2

/obj/item/weapon/implant/emp/activate()
	if (src.uses < 1)	return 0
	src.uses--
	empulse(imp_in, 3, 5)
