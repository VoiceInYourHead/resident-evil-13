/obj/item/documents
	name = "secret documents"
	desc = "\"Top Secret\" documents printed on special copy-protected paper."
	icon = 'icons/obj/bureaucracy.dmi'
	icon_state = "docs_generic"
	item_state = "paper"
	throwforce = 0
	w_class = 1
	throw_range = 1
	throw_speed = 1
	layer = 4
	pressure_resistance = 1

/obj/item/documents/nanotrasen
	desc = "\"Top Secret\" Nanotrasen documents printed on special copy-protected paper. It is filled with complex diagrams and lists of names, dates and coordinates."
	icon_state = "docs_verified"

/obj/item/documents/syndicate
	desc = "\"Top Secret\" documents printed on special copy-protected paper. It details sensitive Syndicate operational intelligence."

/obj/item/documents/syndicate/red
	name = "'Red' secret documents"
	desc = "\"Top Secret\" documents printed on special copy-protected paper. It details sensitive Syndicate operational intelligence. These documents are marked \"Red\"."
	icon_state = "docs_red"

/obj/item/documents/syndicate/blue
	name = "'Blue' secret documents"
	desc = "\"Top Secret\" documents printed on special copy-protected paper. It details sensitive Syndicate operational intelligence. These documents are marked \"Blue\"."
	icon_state = "docs_blue"

/obj/item/documents/resident
	icon = 'icons/stalker/some_stuff/quest.dmi'
	icon_state = "documents_1"
	desc = "Стара&#255; папка с документами. На титульном листе написано: \"Изучение последствий введени&#255; сыворотки в организм\", проект - \"K-04\". Далее идут сотни каких-то формул, размышлений и размытых фотографий. Ничего особенного больше нет."

/obj/item/documents/resident/lvl2
	icon = 'icons/stalker/some_stuff/quest.dmi'
	icon_state = "documents_2"
	desc = "Папка файлов с грифом \"Особой важности\". Согласно документам, в св&#255;зи с финансовым кризисом корпорации, был отдан приказ об закрытии программы \"T-17\" от 08.06.15. Весь персонал об&#255;зан быть транспортирован в \"Резервную Лабораторию\" вместе со всеми образцами и данными об исследовани&#255;х в течение 3х дней. Доктор Арчибальд Стоун должен быть помещён под стражу в пятый блок вплоть до передачи на Объект 141 за дальнейшим изучением дела."

/obj/item/documents/resident/lvl3
	icon = 'icons/stalker/some_stuff/quest.dmi'
	icon_state = "documents_3"
	desc = "Папка файлов с грифом \"Совершенно секретно\". В документах говоритс&#255; об увеличении поставок оборудовани&#255; и дополнительных образцов в некую лабораторию \"K-3\" за счет бюджета \"НИИ Асмодей\". Согласно приказу №61 от 27.01.15 в лабораторию был отправлено большое количество припасов и научного оборудования, из всех списка выдел&#255;ютс&#255;  4 \"Особых образца\" и 2 \"Объекта дл&#255; дополнительного изучени&#255;\"."
