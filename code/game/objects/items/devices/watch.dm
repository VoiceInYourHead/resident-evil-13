/obj/item/device/watch
	name = "Watch"
	desc = "Nice device for check your health."
	icon = 'icons/stalker/lohweb/personal.dmi'
	icon_state = "braceshield"
	item_state = "braceshield"
	w_class = 2
	flags = CONDUCT
	slot_flags = SLOT_BELT
	materials = list(MAT_METAL=50, MAT_GLASS=20)
	action_button_name = "Toggle Light"

