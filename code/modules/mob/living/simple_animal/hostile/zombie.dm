/mob/living/simple_animal/hostile/zombie
	name = "zombie"
	desc = "When Observe is full, the dead shall walk the station."
	icon = 'icons/mob/human.dmi'
	icon_state = "zombie_s"
	icon_living = "zombie_s"
	icon_dead = "zombie_dead"
	turns_per_move = 5
	speak_emote = list("groans")
	emote_see = list("groans")
	a_intent = "harm"
	maxHealth = 150
	health = 150
	speed = 5
	harm_intent_damage = 18
	melee_damage_lower = 40
	melee_damage_upper = 40
	attacktext = "claws"
	attack_sound = 'sound/hallucinations/growl1.ogg'
	atmos_requirements = list("min_oxy" = 0, "max_oxy" = 0, "min_tox" = 0, "max_tox" = 0, "min_co2" = 0, "max_co2" = 5, "min_n2" = 0, "max_n2" = 0)
	minbodytemp = 0
	maxbodytemp = 350
	unsuitable_atmos_damage = 10
	environment_smash = 1
	robust_searching = 1
	stat_attack = 2
	gold_core_spawnable = 1
	faction = list("stalker_mutants1")
	var/mob/living/carbon/human/stored_corpse = null
	var/match = 1
	see_invisible = SEE_INVISIBLE_MINIMUM
	see_in_dark = 2
	layer = MOB_LAYER - 0.1
	move_to_delay = 5.8

/mob/living/simple_animal/hostile/zombie/scientist
	name = "zombie scientist"
	desc = "When Observe is full, the dead shall walk the station."
	icon = 'icons/mob/human.dmi'
	icon_state = "zombiee_s"
	icon_living = "zombiee_s"
	icon_dead = "zombiee_dead"
	turns_per_move = 5
	speak_emote = list("groans")
	emote_see = list("groans")
	a_intent = "harm"
	maxHealth = 150
	health = 150
	speed = 5
	harm_intent_damage = 18
	melee_damage_lower = 40
	melee_damage_upper = 40
	attacktext = "claws"
	attack_sound = 'sound/hallucinations/growl1.ogg'
	atmos_requirements = list("min_oxy" = 0, "max_oxy" = 0, "min_tox" = 0, "max_tox" = 0, "min_co2" = 0, "max_co2" = 5, "min_n2" = 0, "max_n2" = 0)
	minbodytemp = 0
	maxbodytemp = 350
	unsuitable_atmos_damage = 10
	environment_smash = 1
	robust_searching = 1
	stat_attack = 2
	gold_core_spawnable = 1
	faction = list("stalker_mutants1")
	see_invisible = SEE_INVISIBLE_MINIMUM
	see_in_dark = 2
	layer = MOB_LAYER - 0.1
	move_to_delay = 5.8

/mob/living/simple_animal/hostile/zombie/insane
	name = "zombie"
	desc = "When Observe is full, the dead shall walk the station."
	icon = 'icons/mob/human.dmi'
	icon_state = "zombiesh_s"
	icon_living = "zombiesh_s"
	icon_dead = "zombiesh_dead"
	turns_per_move = 3
	speak_emote = list("groans")
	emote_see = list("groans")
	a_intent = "harm"
	maxHealth = 110
	health = 110
	speed = 3
	harm_intent_damage = 14
	melee_damage_lower = 25
	melee_damage_upper = 25
	attacktext = "claws"
	attack_sound = 'sound/hallucinations/growl1.ogg'
	atmos_requirements = list("min_oxy" = 0, "max_oxy" = 0, "min_tox" = 0, "max_tox" = 0, "min_co2" = 0, "max_co2" = 5, "min_n2" = 0, "max_n2" = 0)
	minbodytemp = 0
	maxbodytemp = 350
	unsuitable_atmos_damage = 10
	environment_smash = 1
	robust_searching = 1
	stat_attack = 2
	gold_core_spawnable = 1
	faction = list("stalker_mutants1")
	see_invisible = SEE_INVISIBLE_MINIMUM
	see_in_dark = 2
	layer = MOB_LAYER - 0.1
	move_to_delay = 3


/mob/living/simple_animal/hostile/zombie/policeman
	name = "zombie officer"
	desc = "When Observe is full, the dead shall walk the station."
	icon = 'icons/mob/human.dmi'
	icon_state = "zombiep_s"
	icon_living = "zombiep_s"
	icon_dead = "zombiep_dead"
	turns_per_move = 5
	speak_emote = list("groans")
	emote_see = list("groans")
	a_intent = "harm"
	maxHealth = 250
	health = 250
	speed = 8
	harm_intent_damage = 22
	melee_damage_lower = 55
	melee_damage_upper = 55
	attacktext = "claws"
	attack_sound = 'sound/hallucinations/growl1.ogg'
	atmos_requirements = list("min_oxy" = 0, "max_oxy" = 0, "min_tox" = 0, "max_tox" = 0, "min_co2" = 0, "max_co2" = 5, "min_n2" = 0, "max_n2" = 0)
	minbodytemp = 0
	maxbodytemp = 350
	unsuitable_atmos_damage = 10
	environment_smash = 1
	robust_searching = 1
	stat_attack = 2
	gold_core_spawnable = 1
	faction = list("stalker_mutants1")
	see_invisible = SEE_INVISIBLE_MINIMUM
	see_in_dark = 2
	layer = MOB_LAYER - 0.1
	move_to_delay = 9.5

/mob/living/simple_animal/hostile/zombie/policeman/AttackingTarget(mob/M, mob/user)
	..()
	if(prob(20))
		melee_damage_upper += 10
		melee_damage_lower += 10

/mob/living/simple_animal/hostile/zombie/swat
	name = "heavy armored zombie"
	desc = "When Observe is full, the dead shall walk the station."
	icon = 'icons/mob/human.dmi'
	icon_state = "zombies_s"
	icon_living = "zombies_s"
	icon_dead = "zombies_dead"
	turns_per_move = 5
	speak_emote = list("groans")
	emote_see = list("groans")
	a_intent = "harm"
	maxHealth = 500
	health = 500
	speed = 12
	harm_intent_damage = 32
	melee_damage_lower = 60
	melee_damage_upper = 60
	attacktext = "claws"
	attack_sound = 'sound/hallucinations/growl1.ogg'
	atmos_requirements = list("min_oxy" = 0, "max_oxy" = 0, "min_tox" = 0, "max_tox" = 0, "min_co2" = 0, "max_co2" = 5, "min_n2" = 0, "max_n2" = 0)
	minbodytemp = 0
	maxbodytemp = 350
	unsuitable_atmos_damage = 10
	environment_smash = 1
	robust_searching = 1
	stat_attack = 2
	gold_core_spawnable = 1
	faction = list("stalker_mutants1")
	see_invisible = SEE_INVISIBLE_MINIMUM
	see_in_dark = 2
	match = 0
	layer = MOB_LAYER - 0.1
	move_to_delay = 14

/mob/living/simple_animal/hostile/zombie/swat/AttackingTarget(mob/M, mob/user)
    ..()
    if(prob(20))
        M.Stun(7)

/mob/living/simple_animal/hostile/boss
	name = "zombie officer"
	desc = "When Observe is full, the dead shall walk the station."
	icon = 'icons/mob/human.dmi'
	icon_state = "zombieboss_s"
	icon_living = "zombieboss_s"
	icon_dead = "zombieboss_dead"
	turns_per_move = 5
	speak_emote = list("groans")
	emote_see = list("groans")
	a_intent = "harm"
	maxHealth = 4000
	health = 4000
	speed = 3
	harm_intent_damage = 38
	melee_damage_lower = 60
	melee_damage_upper = 60
	attacktext = "claws"
	attack_sound = 'sound/hallucinations/growl1.ogg'
	atmos_requirements = list("min_oxy" = 0, "max_oxy" = 0, "min_tox" = 0, "max_tox" = 0, "min_co2" = 0, "max_co2" = 5, "min_n2" = 0, "max_n2" = 0)
	minbodytemp = 0
	maxbodytemp = 350
	unsuitable_atmos_damage = 10
	environment_smash = 1
	robust_searching = 1
	stat_attack = 2
	gold_core_spawnable = 1
	faction = list("stalker_mutants1")
	see_invisible = SEE_INVISIBLE_MINIMUM
	see_in_dark = 2
	layer = MOB_LAYER - 0.1
	ranged = 1
	ranged_cooldown = 1 //By default, start the Goliath with his cooldown off so that people can run away quickly on first sight
	ranged_cooldown_cap = 2
	move_to_delay = 3
	var/Xthrow = 1
	var/leaping = 0

/mob/living/simple_animal/hostile/boss/proc/punch(mob/living/target) //Задел на будующее для пинков.
	target.visible_message("<span class='danger'>[target.name] was punched by the [src.name]!</span>", \
		"<span class='userdanger'>You feel a powerful punch which sending your body flying!</span>", \
		"<span class='italics'>You hear a crack!</span>")
	var/atom/throw_target = get_edge_target_turf(target, get_dir(src, get_step_away(target, src)))//Куда летать?
	target.throw_at(throw_target, 200, 4) //Полёт в соседний корридор
	return //Не трогать.

/mob/living/simple_animal/hostile/boss/AttackingTarget()
	..()
	spawn(0)
	if(istype(target, /mob/living)) //Если и менять в будующем, то только используя эти заданные переменные, либо делать для объектов отдельно тип
		var/mob/living/L = target
		if(ishuman(L))
			var/mob/living/carbon/human/M = L //Не трогать эту ступеньку из ссылок!
			if(prob(70)) //Старый шанс стана
				M.Stun(10)
			if(istype(M) && Xthrow == 1) //Проверяет на "человек ли" и включены ли пинки
				punch(M) //Прока на посыл
				M.Stun(3)
				M.Dizzy(10)

/mob/living/simple_animal/hostile/boss/OpenFire()
	if(get_dist(src, target) <= 4)
		leaping = 1
		//throw_at_fast(target, 7, 1)
		throw_at(target, 5, 1, spin=1, diagonals_first = 1)
		leaping = 0
		ranged_cooldown = ranged_cooldown_cap
	return
				//sleep(10)

/mob/living/simple_animal/hostile/boss/throw_impact(atom/A)

	if(!leaping)
		return ..()

	if(A)
		if(istype(A, /mob/living))
			var/mob/living/L = A
			var/blocked = 0
			if(ishuman(A))
				var/mob/living/carbon/human/H = A
				if(H.check_shields(90, "the [name]", src, attack_type = THROWN_PROJECTILE_ATTACK))
					blocked = 1
			if(!blocked)
				L.visible_message("<span class ='danger'>[src] pounces on [L]!</span>", "<span class ='userdanger'>[src] pounces on you!</span>")
				L.Weaken(1)
				//sleep(2)//Runtime prevention (infinite bump() calls on hulks)
				step_towards(src,L)
		else if(A.density && !A.CanPass(src))
			visible_message("<span class ='danger'>[src] smashes into [A]!</span>", "<span class ='alertalien'>[src] smashes into [A]!</span>")
			weakened = 2

		if(leaping)
			leaping = 0
			update_icons()
			update_canmove()


/mob/living/simple_animal/hostile/zombie/AttackingTarget()
	..()
	if(istype(target, /mob/living))
		var/mob/living/L = target
		if(ishuman(L) && L.stat)
			var/mob/living/carbon/human/H = L
			for(var/mob/living/simple_animal/hostile/zombie/holder/Z in H) //No instant heals for people who are already zombies
				src << "<span class='userdanger'>They'll be getting up on their own, just give them a minute!</span>"
				Z.faction = src.faction //Just in case zombies somehow ended up on different "teams"
				H.faction = src.faction
				return
			Zombify(H)
		else if (L.stat) //So they don't get stuck hitting a corpse
			L.gib()
			visible_message("<span class='danger'>[src] tears [L] to pieces!</span>")
			src << "<span class='userdanger'>You feast on [L], restoring your health!</span>"
			src.revive()

/mob/living/simple_animal/hostile/zombie/death()
	..()
	if(stored_corpse)
		stored_corpse.loc = loc
		if(ckey)
			stored_corpse.ckey = src.ckey
			stored_corpse << "<span class='userdanger'>You're down, but not quite out. You'll be back on your feet within a minute or two.</span>"
			var/mob/living/simple_animal/hostile/zombie/holder/D = new/mob/living/simple_animal/hostile/zombie/holder(stored_corpse)
			D.faction = src.faction
		qdel(src)
		return
	src << "<span class='userdanger'>You're down, but not quite out. You'll be back on your feet within a minute or two.</span>"
	spawn(rand(3000, 3400))
		if(src)
			visible_message("<span class='danger'>[src] staggers to their feet!</span>")
			src.revive()

/mob/living/simple_animal/hostile/zombie/proc/Zombify(mob/living/carbon/human/H)
	H.set_species(/datum/species/zombie)
	var/mob/living/simple_animal/hostile/zombie/Z = new /mob/living/simple_animal/hostile/zombie(H.loc)
	Z.faction = src.faction
	Z.appearance = H.appearance
	Z.transform = matrix()
	Z.pixel_y = 0
	for(var/mob/dead/observer/ghost in player_list)
		if(H.real_name == ghost.real_name)
			ghost.reenter_corpse()
			break
	Z.ckey = H.ckey
	H.stat = DEAD
	H.loc = Z
	Z.stored_corpse = H
	for(var/mob/living/simple_animal/hostile/zombie/holder/D in H) //Dont want to revive them twice
		qdel(D)
	visible_message("<span class='danger'>[Z] staggers to their feet!</span>")
	Z << "<span class='userdanger'>You are now a zombie! Follow your creators lead!</span>"


/mob/living/simple_animal/hostile/spawner/zombie
	name = "corpse pit"
	desc = "A pit full of zombies."
	icon_state = "tombstone"
	icon_living = "tombstone"
	icon = 'icons/mob/nest.dmi'
	health = 400
	maxHealth = 400
	list/spawned_mobs = list()
	max_mobs = 40
	spawn_time = 100
	mob_type = /mob/living/simple_animal/hostile/zombie
	spawn_text = "emerges from"
	atmos_requirements = list("min_oxy" = 0, "max_oxy" = 0, "min_tox" = 0, "max_tox" = 0, "min_co2" = 0, "max_co2" = 0, "min_n2" = 0, "max_n2" = 0)
	faction = list("zombie")
	del_on_death = 1

/mob/living/simple_animal/hostile/spawner/zombie/lesser
	name = "lesser corpse pit"
	desc = "A pit full of less zombies."
	max_mobs = 10
	spawn_time = 150
	health = 150
	maxHealth = 150

/mob/living/simple_animal/hostile/spawner/zombie/New()
	..()
	deathmessage = "[src] collapes, stopping the flow of zombies!"


/obj/item/weapon/shovel/cursed
	name = "cursed shovel"
	desc = "You probably shouldn't use this particular shovel."
	var/used = FALSE
	force = 15

/obj/item/weapon/shovel/cursed/attack_self(mob/living/user)
	if(used == FALSE)
		visible_message("<span class='danger'>[user] digs a pit to Hell!</span>")
		new /mob/living/simple_animal/hostile/spawner/zombie(get_turf(src.loc))
		used = TRUE
	else
		user << "The unearthly energies that once powered this shovel are now dormant. Still sharp though."


/mob/living/simple_animal/hostile/zombie/holder
	name = "infection holder"
	icon_state = "none"
	icon_living = "none"
	icon_dead = "none"
	desc = "You shouldn't be seeing this."
	invisibility = 101
	unsuitable_atmos_damage = 0
	stat_attack = 2
	gold_core_spawnable = 0
	AIStatus = AI_OFF
	faction = list("stalker_mutants1")
	stop_automated_movement = 1
	density = 0

/mob/living/simple_animal/hostile/zombie/holder/New()
	..()
	spawn(rand(3000, 3400))
		if(src && istype(loc, /mob/living/carbon/human))
			var/mob/living/carbon/human/H = loc
			Zombify(H)
		qdel(src)

/mob/living/simple_animal/hostile/darkone/weak
	name = "dark one"
	desc = "When Observe is full, the dead shall walk."
	icon = 'icons/stalker/some_stuff/zombie_w.dmi'
	icon_state = "weak2"
	icon_living = "weak2"
	icon_dead = "weak_dead"
	turns_per_move = 3
	speak_emote = list("groans")
	emote_see = list("groans")
	a_intent = "harm"
	maxHealth = 80
	health = 80
	speed = 3
	harm_intent_damage = 20
	melee_damage_lower = 35
	melee_damage_upper = 35
	attacktext = "claws"
	attack_sound = 'sound/hallucinations/growl1.ogg'
	atmos_requirements = list("min_oxy" = 0, "max_oxy" = 0, "min_tox" = 0, "max_tox" = 0, "min_co2" = 0, "max_co2" = 5, "min_n2" = 0, "max_n2" = 0)
	minbodytemp = 0
	maxbodytemp = 350
	unsuitable_atmos_damage = 10
	environment_smash = 1
	robust_searching = 1
	stat_attack = 2
	gold_core_spawnable = 1
	faction = list("stalker_mutants1")
	see_invisible = SEE_INVISIBLE_MINIMUM
	see_in_dark = 2
	layer = MOB_LAYER - 0.1
	move_to_delay = 2

/*/mob/living/simple_animal/hostile/darkone/weak/New()
	icon_state = "weak[rand(1, 4)]"*/

/mob/living/simple_animal/hostile/darkone/standart
	name = "dark one"
	desc = "When Observe is full, the dead shall walk."
	icon = 'icons/stalker/some_stuff/zombie_s.dmi'
	icon_state = "standart5"
	icon_living = "standart5"
	icon_dead = "standart_dead"
	turns_per_move = 3
	speak_emote = list("groans")
	emote_see = list("groans")
	a_intent = "harm"
	maxHealth = 120
	health = 120
	speed = 4
	harm_intent_damage = 25
	melee_damage_lower = 40
	melee_damage_upper = 40
	attacktext = "claws"
	attack_sound = 'sound/hallucinations/growl1.ogg'
	atmos_requirements = list("min_oxy" = 0, "max_oxy" = 0, "min_tox" = 0, "max_tox" = 0, "min_co2" = 0, "max_co2" = 5, "min_n2" = 0, "max_n2" = 0)
	minbodytemp = 0
	maxbodytemp = 350
	unsuitable_atmos_damage = 10
	environment_smash = 1
	robust_searching = 1
	stat_attack = 2
	gold_core_spawnable = 1
	faction = list("stalker_mutants1")
	see_invisible = SEE_INVISIBLE_MINIMUM
	see_in_dark = 2
	layer = MOB_LAYER - 0.1
	move_to_delay = 4

//mob/living/simple_animal/hostile/darkone/standart/New()
//	icon_state = "standart[rand(1, 6)]"

/mob/living/simple_animal/hostile/darkone/ranged
	name = "dark one"
	desc = "When Observe is full, the dead shall walk."
	icon = 'icons/stalker/some_stuff/zombie_s.dmi'
	icon_state = "standartshooter"
	icon_living = "standartshooter"
	icon_dead = "standart_dead"
	turns_per_move = 5
	speak_emote = list("groans")
	emote_see = list("groans")
	projectiletype = /obj/item/projectile/bullet/bulletmag44
	projectilesound = 'sound/stalker/weapons/colt1911_shot.ogg'
	ranged = 1
	ranged_message = "shoots"
	ranged_cooldown_cap = 5
	a_intent = "harm"
	maxHealth = 150
	health = 150
	speed = 5
	harm_intent_damage = 15
	melee_damage_lower = 25
	melee_damage_upper = 25
	attacktext = "claws"
	attack_sound = 'sound/hallucinations/growl1.ogg'
	atmos_requirements = list("min_oxy" = 0, "max_oxy" = 0, "min_tox" = 0, "max_tox" = 0, "min_co2" = 0, "max_co2" = 5, "min_n2" = 0, "max_n2" = 0)
	minbodytemp = 0
	maxbodytemp = 350
	unsuitable_atmos_damage = 10
	environment_smash = 1
	robust_searching = 1
	stat_attack = 2
	gold_core_spawnable = 1
	faction = list("stalker_mutants1")
	see_invisible = SEE_INVISIBLE_MINIMUM
	see_in_dark = 2
	layer = MOB_LAYER - 0.1
	move_to_delay = 5

/mob/living/simple_animal/hostile/darkone/ranged/heavy
	name = "dark one"
	desc = "When Observe is full, the dead shall walk."
	icon = 'icons/stalker/some_stuff/zombie_s.dmi'
	icon_state = "standartshooter2"
	icon_living = "standartshooter2"
	icon_dead = "standart_dead"
	turns_per_move = 5
	speak_emote = list("groans")
	emote_see = list("groans")
	projectiletype = /obj/item/projectile/bullet/bullet12x70
	projectilesound = 'sound/stalker/weapons/spsa_shot.ogg'
	ranged = 1
	ranged_message = "shoots"
	ranged_cooldown_cap = 20
	a_intent = "harm"
	maxHealth = 150
	health = 150
	speed = 5
	harm_intent_damage = 15
	melee_damage_lower = 25
	melee_damage_upper = 25
	attacktext = "claws"
	attack_sound = 'sound/hallucinations/growl1.ogg'
	atmos_requirements = list("min_oxy" = 0, "max_oxy" = 0, "min_tox" = 0, "max_tox" = 0, "min_co2" = 0, "max_co2" = 5, "min_n2" = 0, "max_n2" = 0)
	minbodytemp = 0
	maxbodytemp = 350
	unsuitable_atmos_damage = 10
	environment_smash = 1
	robust_searching = 1
	stat_attack = 2
	gold_core_spawnable = 1
	faction = list("stalker_mutants1")
	see_invisible = SEE_INVISIBLE_MINIMUM
	see_in_dark = 2
	layer = MOB_LAYER - 0.1
	move_to_delay = 5