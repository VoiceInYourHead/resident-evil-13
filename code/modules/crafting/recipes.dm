
/datum/table_recipe
	var/name = "" //in-game display name
	var/reqs[] = list() //type paths of items consumed associated with how many are needed
	var/result //type path of item resulting from this craft
	var/tools[] = list() //type paths of items needed but not consumed
	var/time = 30 //time in deciseconds
	var/parts[] = list() //type paths of items that will be placed in the result
	var/chem_catalysts[] = list() //like tools but for reagents
	var/category = CAT_NONE //where it shows up in the crafting UI


/datum/table_recipe/gg
	name = "Combination of two green herbs"
	result = /obj/item/weapon/reagent_containers/pill/stalker/trava/gg
	reqs = list(/obj/item/weapon/reagent_containers/pill/stalker/trava/green = 2)
	time = 20
	category = CAT_MEDICAL

/datum/table_recipe/gb
	name = "Combination of green and blue herbs"
	result = /obj/item/weapon/reagent_containers/pill/stalker/trava/gb
	reqs = list(/obj/item/weapon/reagent_containers/pill/stalker/trava/green = 1,
	/obj/item/weapon/reagent_containers/pill/stalker/trava/blue = 1)
	time = 20
	category = CAT_MEDICAL

/datum/table_recipe/rg
	name = "Combination of red and green herbs"
	result = /obj/item/weapon/reagent_containers/pill/stalker/trava/rg
	reqs = list(/obj/item/weapon/reagent_containers/pill/stalker/trava/green = 1,
	/obj/item/weapon/reagent_containers/pill/stalker/trava/red = 1)
	time = 20
	category = CAT_MEDICAL

/datum/table_recipe/ggg
	name = "Combination of three green herbs"
	result = /obj/item/weapon/reagent_containers/pill/stalker/trava/ggg
	reqs = list(/obj/item/weapon/reagent_containers/pill/stalker/trava/green = 3)
	time = 40
	category = CAT_MEDICAL

/datum/table_recipe/rgb
	name = "Combination of red, green and blue herbs"
	result = /obj/item/weapon/reagent_containers/pill/stalker/trava/rgb
	reqs = list(/obj/item/weapon/reagent_containers/pill/stalker/trava/green = 1,
	/obj/item/weapon/reagent_containers/pill/stalker/trava/red = 1,
	/obj/item/weapon/reagent_containers/pill/stalker/trava/blue = 1)
	time = 40
	category = CAT_MEDICAL


/datum/table_recipe/pammosmall
	name = "15x pistol ammo"
	result = /obj/item/ammo_box/stalker/bmag44
	reqs = list(/obj/item/weapon/metro/loot/craft/bluepowder = 2)
	time = 30
	category = CAT_AMMO

/datum/table_recipe/pammostandart
	name = "30x pistol ammo"
	result = /obj/item/weapon/storage/box/metro/pistol_44_mag_small
	reqs = list(/obj/item/weapon/metro/loot/craft/redpowder = 1,
	/obj/item/weapon/metro/loot/craft/bluepowder = 1)
	time = 30
	category = CAT_AMMO

/datum/table_recipe/pammobig
	name = "60x pistol ammo"
	result = /obj/item/weapon/storage/box/metro/pistol_44_mag
	reqs = list(/obj/item/weapon/metro/loot/craft/redpowder = 2)
	time = 30
	category = CAT_AMMO

/datum/table_recipe/sammosmall
	name = "8x shotgun ammo"
	result = /obj/item/ammo_box/stalker/b12x70
	reqs = list(/obj/item/weapon/metro/loot/craft/bluepowder = 1,
	/obj/item/weapon/metro/loot/craft/yellowpowder = 1)
	time = 30
	category = CAT_AMMO

/datum/table_recipe/sammostandart
	name = "16x shotgun ammo"
	result = /obj/item/weapon/storage/box/metro/shotgun_b12x70_small
	reqs = list(/obj/item/weapon/metro/loot/craft/redpowder = 1,
	/obj/item/weapon/metro/loot/craft/yellowpowder = 1)
	time = 30
	category = CAT_AMMO

/datum/table_recipe/sammobig
	name = "40x shotgun ammo"
	result = /obj/item/weapon/storage/box/metro/shotgun_b12x70
	reqs = list(/obj/item/weapon/metro/loot/craft/yellowpowder = 2)
	time = 30
	category = CAT_AMMO

/datum/table_recipe/rammo
	name = "36x rifle ammo"
	result = /obj/item/weapon/storage/box/metro/rifle_545x39_small
	reqs = list(/obj/item/weapon/metro/loot/craft/whitepowder = 2)
	time = 30
	category = CAT_AMMO

/datum/table_recipe/bint
	name = "Guaze"
	result = /obj/item/stack/medical/bintik
	reqs = list(/obj/item/weapon/metro/loot/craft/spirt = 1,
	/obj/item/weapon/metro/loot/craft/tryapka = 1)
	time = 25
	category = CAT_MEDICAL

/datum/table_recipe/metro/battlebarrel
	name = "Battle Barrel"
	result = /obj/item/weapon/melee/classic_baton/barrel
	reqs = list(/obj/item/weapon/metro/loot/craft/barrel = 2,
	/obj/item/weapon/metro/loot/craft/tape = 1)
	time = 50
	category = CAT_WEAPON

/datum/table_recipe/metro/barrel
	name = "Barrel"
	result = /obj/item/weapon/metro/loot/craft/barrel
	reqs = list(/obj/item/weapon/metro/loot/craft/metall = 2,
	/obj/item/weapon/metro/loot/craft/glue = 1)
	time = 25
	category = CAT_MISC

/datum/table_recipe/metro/knife
	name = "Makeshift Knife"
	result = /obj/item/weapon/melee/knife/zatochka
	reqs = list(/obj/item/weapon/metro/loot/craft/knifeblade = 1,
	/obj/item/weapon/metro/loot/craft/tape = 1,
	/obj/item/weapon/metro/loot/craft/handle = 1)
	time = 30
	category = CAT_WEAPON

/datum/table_recipe/metro/knifeblade
	name = "Blade"
	result = /obj/item/weapon/metro/loot/craft/knifeblade
	reqs = list(/obj/item/weapon/metro/loot/craft/metall = 1)
	time = 30
	category = CAT_MISC

/datum/table_recipe/metro/upgrdbattlebarrel
	name = "Upgraded Battle Barrel"
	result = /obj/item/weapon/melee/classic_baton/barrel/dmg
	reqs = list(/obj/item/weapon/melee/classic_baton/barrel = 1,
	/obj/item/weapon/metro/loot/craft/glue = 1,
	/obj/item/weapon/metro/loot/craft/chain = 1)
	time = 30
	category = CAT_WEAPONMOD

/datum/table_recipe/metro/surv
	name = "Survivor implant"
	result = /obj/item/weapon/implanter/survivor
	reqs = list(/obj/item/weapon/implanter/med = 1,
	/obj/item/weapon/metro/loot/craft/upgrader/condition_and_accuracy = 1)
	time = 50
	category = CAT_MISC

/datum/table_recipe/metro/med
	name = "Medicine implant"
	result = /obj/item/weapon/implanter/med
	reqs = list(/obj/item/weapon/metro/loot/craft/upgrader/health_and_armor = 1)
	time = 30
	category = CAT_MISC

/datum/table_recipe/metro/speed
	name = "Speedster implant"
	result = /obj/item/weapon/implanter/speed
	reqs = list(/obj/item/weapon/metro/loot/craft/upgrader/condition_and_accuracy = 1)
	time = 30
	category = CAT_MISC

/datum/table_recipe/metro/upgrdwrench
	name = "Wrench damage upgrade"
	result = /obj/item/weapon/melee/wrench/dmg
	reqs = list(/obj/item/weapon/metro/loot/craft/upgrader/damage = 1,
	/obj/item/weapon/melee/wrench = 1)
	time = 40
	category = CAT_WEAPONMOD

/datum/table_recipe/metro/upgrdsword
	name = "Sword damage upgrade"
	result = /obj/item/weapon/melee/knife/battle_axe/dmg
	reqs = list(/obj/item/weapon/metro/loot/craft/upgrader/damage = 1,
	/obj/item/weapon/melee/knife/battle_axe = 1)
	time = 40
	category = CAT_WEAPONMOD

/datum/table_recipe/metro/upgrdaxe
	name = "Axe damage upgrade"
	result = /obj/item/weapon/twohanded/fireaxe/re/dmg
	reqs = list(/obj/item/weapon/metro/loot/craft/upgrader/damage = 1,
	/obj/item/weapon/twohanded/fireaxe/re = 1)
	time = 40
	category = CAT_WEAPONMOD

/datum/table_recipe/metro/revolvercond
	name = "Revolver condition upgrade"
	result = /obj/item/weapon/gun/projectile/revolver/metro/revolver/cond
	reqs = list(/obj/item/weapon/metro/loot/craft/upgrader/condition_and_accuracy = 1,
	/obj/item/weapon/gun/projectile/revolver/metro/revolver = 1)
	time = 50
	category = CAT_WEAPONMOD

/datum/table_recipe/metro/dbcond
	name = "Double-barrel shotgun condition upgrade"
	result = /obj/item/weapon/gun/projectile/revolver/metro/douplet/cond
	reqs = list(/obj/item/weapon/metro/loot/craft/upgrader/condition_and_accuracy = 1,
	/obj/item/weapon/gun/projectile/revolver/metro/douplet = 1)
	time = 50
	category = CAT_WEAPONMOD

/datum/table_recipe/metro/cscond
	name = "Combat shotgun condition upgrade"
	result = /obj/item/weapon/gun/projectile/shotgun/combatshotgun/cond
	reqs = list(/obj/item/weapon/metro/loot/craft/upgrader/condition_and_accuracy = 1,
	/obj/item/weapon/gun/projectile/shotgun/combatshotgun = 1)
	time = 50
	category = CAT_WEAPONMOD

/datum/table_recipe/metro/coltcond
	name = "Colt condition upgrade"
	result = /obj/item/weapon/gun/projectile/automatic/pistol/cora/cond
	reqs = list(/obj/item/weapon/metro/loot/craft/upgrader/condition_and_accuracy = 1,
	/obj/item/weapon/gun/projectile/automatic/pistol/cora = 1)
	time = 50
	category = CAT_WEAPONMOD

/datum/table_recipe/metro/smgcond
	name = "M33 condition upgrade"
	result = /obj/item/weapon/gun/projectile/automatic/scar/cond
	reqs = list(/obj/item/weapon/metro/loot/craft/upgrader/condition_and_accuracy = 1,
	/obj/item/weapon/gun/projectile/automatic/scar = 1)
	time = 50
	category = CAT_WEAPONMOD

/datum/table_recipe/metro/vesthp
	name = "Millitary vest armor upgrade"
	result = /obj/item/clothing/suit/metro/armveststandarthp
	reqs = list(/obj/item/weapon/metro/loot/craft/upgrader/health_and_armor = 1,
	/obj/item/clothing/suit/metro/armveststandart = 1)
	time = 50
	category = CAT_EQUIPMOD

/datum/table_recipe/metro/vestcond
	name = "Millitary vest condition upgrade"
	result = /obj/item/clothing/suit/metro/armveststandartcond
	reqs = list(/obj/item/weapon/metro/loot/craft/upgrader/condition_and_accuracy = 1,
	/obj/item/clothing/suit/metro/armveststandart = 1)
	time = 50
	category = CAT_EQUIPMOD

/datum/table_recipe/metro/vesthpcond
	name = "Millitary vest(armor) condition upgrade"
	result = /obj/item/clothing/suit/metro/armveststandarthpcond
	reqs = list(/obj/item/weapon/metro/loot/craft/upgrader/condition_and_accuracy = 1,
	/obj/item/clothing/suit/metro/armveststandarthp = 1)
	time = 50
	category = CAT_EQUIPMOD

/datum/table_recipe/metro/vestcondhp
	name = "Millitary vest(condition) armor upgrade"
	result = /obj/item/clothing/suit/metro/armveststandartcondhp
	reqs = list(/obj/item/weapon/metro/loot/craft/upgrader/health_and_armor = 1,
	/obj/item/clothing/suit/metro/armveststandartcond = 1)
	time = 50
	category = CAT_EQUIPMOD

/datum/table_recipe/metro/axe
	name = "Makeshift Axe"
	result = /obj/item/weapon/twohanded/fireaxe/re
	reqs = list(/obj/item/weapon/metro/loot/craft/axe = 1,
	/obj/item/weapon/metro/loot/craft/tape = 2,
	/obj/item/weapon/metro/loot/craft/handle = 2,
	/obj/item/weapon/metro/loot/craft/barrel = 1)
	time = 80
	category = CAT_WEAPON

/datum/table_recipe/metro/axeb
	name = "Axe blade"
	result = /obj/item/weapon/metro/loot/craft/axe
	reqs = list(/obj/item/weapon/metro/loot/craft/metall = 2)
	time = 30
	category = CAT_MISC
