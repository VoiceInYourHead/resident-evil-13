#if !defined(MAP_FILE)

        #include "map_files\re\admin_place.dmm"
        #include "map_files\Metro\test_map_roles.dm"

        #define MAP_FILE "admin_place.dmm"
        #define MAP_NAME "Shooting Range"
		#define MAP_TRANSITION_CONFIG list(MAIN_STATION = CROSSLINKED, CENTCOMM = SELFLOOPING)
#elif !defined(MAP_OVERRIDE)

#warn a map has already been included, ignoring admin_place.dmm.

#endif