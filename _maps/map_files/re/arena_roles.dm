/datum/job/player
	title = "Survivor"
	faction_s = "Civilian"
	faction = "Station"
	total_positions = 5
	spawn_positions = 5
	supervisors = ""
	selection_color = "#dddddd"
	access = list()			//See /datum/job/assistant/get_access()
	minimal_access = list()	//See /datum/job/assistant/get_access()
	whitelist_only = 0
	outfit = /datum/outfit/job/player

/datum/outfit/job/player
	name = "Survivor"
	faction_s = "Civilian"

/datum/outfit/job/player/pre_equip(mob/living/carbon/human/H)
	..()
	uniform = /obj/item/clothing/under/metro/civilian_fatigues6
	suit = pick(/obj/item/clothing/suit/metro/armveststandart,/obj/item/clothing/suit/metro/labcoat)
	ears = null
	id = /obj/item/weapon/card/id/metro/trc
	belt = /obj/item/weapon/storage/belt/metro/pouch
	mask = null
	gloves = /obj/item/clothing/gloves/fingerless
	shoes = /obj/item/clothing/shoes/jackboots/warm
	suit_store =pick(/obj/item/weapon/gun/projectile/revolver/metro/revolver, /obj/item/weapon/gun/projectile/automatic/pistol/cora)
	back = /obj/item/weapon/storage/backpack/satchel/stalker/civilian
	backpack_contents = list(/obj/item/weapon/storage/box/metro/pistol_44_mag = 2, /obj/item/stack/medical/bintik = 1, /obj/item/device/flashlight/seclite = 1)
	r_pocket =pick(/obj/item/weapon/reagent_containers/pill/stalker/morphine_plus,/obj/item/weapon/storage/box/metro/rifle_545x39_small,/obj/item/weapon/storage/box/MRE)

/datum/outfit/umbrellasoldier  // For select_equipment
	name = "Umbrella Operative"

	uniform = /obj/item/clothing/under/metro/civilian_fatigues4
	suit = /obj/item/clothing/suit/metro/armveststandart
	id = /obj/item/weapon/card/id/metro/stalker
	belt = /obj/item/weapon/storage/belt/metro/pouch
	gloves = /obj/item/clothing/gloves/fingerless
	shoes = /obj/item/clothing/shoes/jackboots/warm
	back = /obj/item/weapon/storage/backpack/satchel/stalker/civilian
	suit_store = /obj/item/weapon/gun/projectile/automatic/scar
	backpack_contents = list(/obj/item/weapon/gun/projectile/automatic/pistol/cora = 1, /obj/item/weapon/reagent_containers/pill/stalker/spray = 1, /obj/item/weapon/storage/box/metro/pistol_44_mag = 2, /obj/item/ammo_box/magazine/metro/scarh = 1, /obj/item/device/flashlight/seclite = 1)
	r_pocket = /obj/item/device/radio
	l_pocket = /obj/item/weapon/storage/box/metro/ifak
	mask = /obj/item/clothing/mask/gas/re
	head = /obj/item/clothing/head/metro/tactical