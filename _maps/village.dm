#if !defined(MAP_FILE)

        #include "map_files\re\village\main_1.dmm"
        #include "map_files\re\village\main_2.dmm"
        #include "map_files\re\village\village_roles.dm"

        #define MAP_FILE "main_1.dmm"
        #define MAP_NAME "Village"
		#define MAP_TRANSITION_CONFIG list(MAIN_STATION = CROSSLINKED, CENTCOMM = SELFLOOPING)
#elif !defined(MAP_OVERRIDE)

#warn a map has already been included, ignoring main_1.dmm.

#endif